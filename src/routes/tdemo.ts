import * as express from 'express'; 
import { IRoute, Router } from 'express-serve-static-core'; 
let router:Router = express.Router(); /* GET home page. */ 
console.log('Router:::', router);
router.get('/', function(req, res, next) { res.render('index', { title: 'Express' }); }); 
module.exports = router;