var db = require("../config/db");
var token = require('../token');
var mod = {
    // 获取部门列表
    getDepartmentList: function(req, res) {

        var sql = 'select * from department';
        var sql1 = 'SELECT * FROM department d LEFT JOIN (SELECT COUNT(*) as membersNum,departmentId FROM users WHERE role = 0 GROUP BY departmentId) c ON d.id=c.departmentId';
        db.query(sql1, function(err, rows) {
            if(err) {

            } else {
                console.log('getDepartmentList:::', rows);
                res.json({
                    returnCode: 200,
                    data: rows,
                    message: '成功！'
                });
            }
        });
        
        
    },

    // 创建部门
    createDepartment: function(req, res) {
        var params = [];
        console.log('createDepartment req.body:::', req.body);
        params.push(req.body.departmentName);
        params.push(req.body.departmentDesc);
        params.push(new Date());
       let sql = 'select * from department where departmentName=?';
       db.select(sql, params, function(err, rows) {
            if(err) {

            } else {
                console.log('createDepartment:::', rows);
                
                
                if(rows.length == 0) {
                    console.log('createDepartment:::', typeof(rows));
                    const inserSql = 'insert into department (departmentName, departmentDesc, createTime) values (?, ?, ?)';
                    db.insert(inserSql, params, function(err1, rows1) {
                        if(err1) {

                        } else {
                            console.log('rows1:::', rows1);
                            res.json({
                                returnCode: 200,
                                data: rows1,
                                message: '创建成功！'
                            });
                        }
                    });
                } else if(rows.length > 0) {
                    res.json({
                        returnCode: 201,
                        message: '部门已存在！'
                    });
                }
                
            }
        });
    },

    deleteDepartmentById: function(req, res) {
        var params = [];
        console.log('createDepartment req.body:::', req.body);
        params.push(req.body.id);
        const sql = 'delete from department where id = ?';
        db.del(sql, params, function(err, rows) {
            if(err) {

            } else {
                res.json({
                    returnCode: 200,
                    message: '删除成功！'
                });
            }
        })
    },
    
    updateDepartmentById: function(req, res) {
        let sql = 'update department set '
        const params = [];
        const obj = req.body;
        const id = obj.id;
        for(let key in obj) {
            if(key != 'id') {
                sql += key + '= ?,';
                params.push(obj[key]);
            }
        }
        sql = sql.slice(0, -1);
        sql += 'where id =?';
        params.push(id);
        db.update(sql, params, function(err, rows) {
            if(err) {
                console.error('err::', err);
            } else {
                res.json({
                    returnCode: 200,
                    message: '更新成功！'
                });
            }
        });
    }
}

module.exports = mod;