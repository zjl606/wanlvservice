var db = require("../config/db");
var token = require('../token');
var mod = {
    register: function(req, res) {
        console.log('req:body::', req.body);
        // var sql = 'insert into user (username, password) values (222, 111)';
        // db.query(sql, function(err, rows) {
        //     if(err) {
        //         console.log('err::', err);
        //     } else {
        //         console.log('rows:::', rows);
        //     }
        // });

        var sql = 'insert into users (username, password) values (?, ?)';
        var selSql = 'select username from user where username=?';
        var params = [];
        var sel = [];
        params.push(req.body.username);
        params.push(req.body.password);
        sel.push(req.body.username);
        console.log('params:::', params);
        
        db.select(selSql, sel, function(err, rows) {
            if(err) {

            } else {
                console.log('select:::', rows);
                if(rows.length == 0) {
                    db.insert(sql, params, function(err, rows) {
                        if(err) {
                            console.log('err::', err);
                        } else {
                            console.log('rows:::', rows);
                            var userToken = token.createToken();
                            res.json({returnCode: 200, token: userToken});
                        }
                    });
                } else if(rows.length > 0) {
                    res.json({
                        returnCode: 201,
                        message: '用户名已存在！'
                    });
                }
            }
        });
        
        
    },

    login: function(req, res) {
        var params = [];
        params.push(req.body.phone);
       let sql = 'select * from users where role=1 and phone=?';
       db.select(sql, params, function(err, rows) {
            if(err) {

            } else {
                console.log('login:::', rows);
                console.log('login:::', typeof(rows));
                
                if(rows.length == 0) {
                    res.json({
                        returnCode: 201,
                        message: '账号不存在！'
                    });
                } else if(rows.length > 0) {
                    var userToken = token.createToken();
                    console.log('rows0', rows[0]);
                    if(rows[0].password == req.body.password) {
                        res.json({
                            returnCode: 200,
                            data: rows,
                            token: userToken,
                            message: '登录成功！'
                        });
                    } else {
                        res.json({
                            returnCode: 201,
                            message: '账号密码错误！'
                        });
                    }
                    
                }
                
            }
        });
    },

    loginAdmin: function(req, res) {
        var selSql = 'select phone, password from users where phone=? and password=? and role = 0';
        var params = [];
        var sel = [];
        params.push(req.body.phone);
        params.push(req.body.password);
        console.log('login:params:::', params);
        db.select(selSql, params, function(err, rows) {
            if(err) {

            } else {
                console.log('login:::', rows);
                if(rows.length == 0) {
                    res.json({
                        returnCode: 201,
                        message: '账号密码错误！'
                    });
                } else if(rows.length > 0) {
                    var userToken = token.createToken();
                    res.json({
                        returnCode: 200,
                        data: rows,
                        token: userToken,
                        message: '登录成功！'
                    });
                }
                
            }
        });
    },

    // 查询用户列表
    getUserList(req, res) {
        var sql = 'select * from users where role = 1';
        var params = [];
        if(req.query.userCode) {
            sql += 'and userCode=?';
            params.push(req.query.userCode);
        }
        if(req.query.phone) {
            sql += 'and phone=?';
            params.push(req.query.phone);
        }
        db.select(sql, params, function(err, rows) {
            if(err) {
                res.json({
                    returnCode: 400,
                    error: err,
                    message: '数据库出错！'
                });
            } else {
                // console.log('login:::', rows);
                res.json({
                    returnCode: 200,
                    data: rows,
                    message: '成功！'
                });
            }
        });
    },

    getStaffList(req, res) {
        // let sql = 'select * from users where role = 0';
        let sql = 'SELECT u.*, d.departmentName FROM users u LEFT JOIN department d ON d.id=u.departmentId WHERE u.role = 0;'
        var params = [];
        // const obj = req.query;
        // for(let key in obj) {
        //     sql += 'and ' + key +'=?';
        //     params.push(obj[key]);
        // }
        db.select(sql, params, function(err, rows) {
            if(err) {
                res.json({
                    returnCode: 400,
                    error: err,
                    message: '数据库出错！'
                });
            } else {
                // console.log('login:::', rows);
                res.json({
                    returnCode: 200,
                    data: rows,
                    message: '成功！'
                });
            }
        });
    },

    // 添加员工
    createStaff(req, res) {
        let sql = 'insert into users (phone, name, departmentId, password, role) values (?, ?, ?, ?, 0)';
        console.log('req.body:::::stafff', req.body);
        if(req.body.phone && req.body.name && req.body.departmentId && req.body.password) {
            const params = [];
            params.push(req.body.phone);
            params.push(req.body.name);
            params.push(req.body.departmentId);
            params.push(req.body.password);
            db.insert(sql, params, function(err, rows) {
                if(err) {
                    res.json({
                        returnCode: 401,
                        message: '添加失败',
                    });
                } else {
                    res.json({
                        returnCode: 200,
                        message: '添加成功!'
                    });
                }
            });
        } else {
            res.json({
                returnCode: 201,
                message: '缺少参数!'
            });
        }
        
    }
}

module.exports = mod;