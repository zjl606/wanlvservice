var mod ={
    user: require('./user'),
    department: require('./department'),
    upload: require('./upload')
}

module.exports = mod;