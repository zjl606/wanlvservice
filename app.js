var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');

// var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');
var routes = require('./routes');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser('sessiontest'));
// app.use(session({
//   secret: 'sessiontest',  //与cookieParser中的一致
//   resave: true,
//   saveUninitialized:true,
//   cookie: {
//     maxAge: 24*60*60*1000  // 有效期，单位是毫秒
//   }
// }));
// app.use(express.static(path.join(__dirname, 'public')));

// var allowCrossDomain = function (req, res, next) {
//   console.log('req::::::', req);
//   res.header('Access-Control-Allow-Origin', '*');//自定义中间件，设置跨域需要的响应头。
//   res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
//   res.header('Access-Control-Allow-Headers', 'Content-Type');
//   res.header('Access-Control-Allow-Credentials','true');
//   res.header('Access-Control-Max-Age','86400');
//   res.header('Access-Control-Allow-Headers','X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
//   next();
//  };
// app.use(allowCrossDomain);//运用跨域的中间件
//设置允许跨域访问该服务.
app.all('*', function (req, res, next) {
  console.log('req.mehtod:::',req.method);
  // res.header('Access-Control-Allow-Origin', 'http://localhost:8080');
  res.header('Access-Control-Allow-Origin', '*');
  // res.header('access-control-request-headers', '*');
  //Access-Control-Allow-Headers ,可根据浏览器的F12查看,把对应的粘贴在这里就行
  // res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.header('Access-Control-Allow-Headers', 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type');
  res.header('Access-Control-Allow-Methods', '*');
  res.header('Content-Type', 'application/json;charset=utf-8');
  next();
});

// app.use('/', indexRouter);
// app.use('/users', usersRouter);
routes(app, logger);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
